

#' titre f_ivna_pour_gg9
#'
#' @param ivna titre
#' @param pdom pdom
#' @param pmillesime pmillesime
#'
#' @return objet ggplot2
#' @export
#'
f_ivna_pour_gg9  <-  function(ivna,pdom,pmillesime){
  ivna %>%
    dplyr::filter(
      dom == pdom,
      pmillesime-7 < annee, annee < pmillesime+1,
      genre %in%  genres_by_code(c("BUS")))
}





#' titre gg9
#'
#' @param title titre
#' @param data chroniques à grapher
#'
#' @return objet ggplot2
#' @export
#'
gg9  <- function(title,data){
  ## {#histo_stack: IVN<annee<(VASP,bus)}
  subtitle <- ""
  cat(glue::glue("legende-image::{title} {subtitle}"))

  data%>%
    ggplot2::ggplot() +
      ggplot2::aes(x=annee_char, y=eff, fill=genre, label=eff) +
      ggplot2::geom_col()+
    ggplot2::geom_text(position = ggplot2::position_stack(vjust=0.5)) +
    ggplot2::labs(title="",
               subtitle = "",
               x="",
               y="",
               legend="")+
    ggplot2::theme(
                legend.title=ggplot2::element_blank())+
    ggplot2::scale_fill_manual(
      "",
      values = c("Autobus et autocar"= "#00bfc4"))
}




