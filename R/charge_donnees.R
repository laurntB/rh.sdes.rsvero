
#' options de configuration par défault de la fonction chargement de données
#'
#' @encoding UTF-8
#'
retrieving_data_options.default_settings <- list(
  cachefile_params = list(
    cachefilename = 'donneesPreparees.rds'
  ),
  datasource_params = list(
    db1path = "ivn-genre-mensuel.sqlite",
    db1tables = c("genre_w","11IR1","dvasp_w"),
    db2path = "ivn-vp-mensuel.sqlite",
    db2tables = c("vp","nrjcat"),
    poppath = "pop.RDS",
    datan_2if1 = "mef-2if1.xls",
    pdom = 'Martinique'
  ),
  retrieving_path_mode = 'relative'
)



#' Chargement des données
#'
#' Mets à disposition des chroniques : ivn, dvp, dvasp, dvp_sl (et aussi dvp_sl_wf)
#' et par confort de l'objet : pop (recensement de la population)
#'
#'
#' @encoding UTF-8
#'
#' @param use_cache si vrai utilise les données en cache, si faux recréer les données depuis les différentes sources
#' @param options ensemble d'options définissant les sources de données
#'
#' @return list(ivn, dvp, dvasp, dvp_sl, dvp_sl_wf, pop)
#' @export
#'
charge_donnees <- function(use_cache = TRUE,
                           options = retrieving_data_options.default_settings){


  exec_mode <- options$retrieving_path_mode
  helperFunctions <- configure_path_helperFunctions(exec_mode, mode_option)
  path.mgr <- helperFunctions$path.mgr
  path.near <- helperFunctions$path.near

  cflep <- options$cachefile_params
  dsrcep <- options$datasource_params

  ## Switch Cache / Pas cache
  if(use_cache){
    donnees_de_base <- loadfromcache_ivn(cflep$cachefilename, path.near)

  }else{
    donnees_de_base <- regen_ivn(dsrcep$db1path, dsrcep$db1tables,
                                 dsrcep$db2path, dsrcep$db2tables,
                                 dsrcep$poppath,
                                 dsrcep$datan_2if1,
                                 dsrcep$pdom,
                                 path.mgr,
                                 path.near)
  }

  donnees_de_base
}
